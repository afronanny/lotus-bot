var fs = require('fs');
var _cookies;

steam64ToUsername = function(steam64) {
	//Validate that the passed ID is valid
	// - Skipping this part for the time being

	var url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/";
	url += ("?key=" + apiKey);
	url += ("&steamids=" + steam64);

	//Send the request to the steam server
	//console.log(url);

	//https://www.npmjs.com/package/request-sync

	var request = require('sync-request');

	var res = request("GET", url);
	var body = JSON.parse(res.getBody("UTF-8"));
	var username = "";
	if(body.response.players[0] != undefined){
		username = body.response.players[0].personaname;
	}
	else {
		return "";
	}

	//console.log(username);

	//Return the username
	return username;
}

parseCookies = function(cookies){
	var result = {};

	var patternName = /(\w*)(?=\=)/g;
    var patternValue = /(?:\=)(.*)$/g;

    for (var i = 0; i < cookies.length; i++) {
      var tempCookieName = cookies[i].match(patternName)[0];
      var tempCookieValue = cookies[i].match(patternValue)[0].slice(1);
      result[tempCookieName] = tempCookieValue;
      //console.log("New cookie: " + tempCookieName + "|" + tempCookieValue + "|" + cookies[i]);
    }

    console.log("Done parsing cookies...");

    _cookies = result;

	return result;
}

newEvent = function(args){
	var result = {};
	console.log(args);

	var group = JSON.parse(fs.readFileSync('config.json')).steam.groupName;

	postData(
		"steamcommunity.com",
		("/groups/" + group + "/eventEdit"),
		{
			'eventID':args[0],
      'name':args[1],
      'serverIP':'127.0.0.1',
      'serverPassword':'test1234',
      'notes':'Testing...123454321',
      'sessionid':_cookies["sessionid"],
      'steamLogin':_cookies["steamLogin"],
      // 'action':'newEvent',
      'action':'updateEvent',
      'tzOffset':'-14400',
      'type':'GameEvent',
      'appID':'440',
      'startDate':'MM/DD/YY',
		  'startHour':'12',
		  'startMinute':'00',
		  'startAMPM':'PM',
		  'timeChoice':'quick',
		  'eventQuickTime':'now',
  	},
  	function(body, index){
  		console.log("Body:" + body.length);
			console.log(body);

  		//console.log("Done...");

  		/*var fs = require('fs');

  		var name = Math.floor(Math.random()*1000);*/

  		var current = fs.readFileSync('log-0.html');

			fs.writeFile(('log-' + index + '.html'),(current + body),function(err) {
				if(err){
					console.log(err);
				} else {
					console.log('Saved log file in "log-' + index + '.html"');
				}
			});
  	}
	);

	// postData(
	// 	"localhost",
	// 	"/lotus/index.php",
	// 	{
	// 		'eventID':'800868224041458443',
  //     'name':'Nope1234',
  //     'serverIP':'127.0.0.1',
  //     'serverPassword':'test1234',
  //     'notes':'Testing...123454321',
  //     'sessionid':_cookies["sessionid"],
  //     'steamLogin':_cookies["steamLogin"],
  //     'action':'newEvent',
  //     'tzOffset':'-14400',
  //     'type':'GameEvent',
  //     'appID':'440',
  //     'startDate':'MM/DD/YY',
	// 	  'startHour':'12',
	// 	  'startMinute':'00',
	// 	  'startAMPM':'PM',
	// 	  'timeChoice':'quick',
	// 	  'eventQuickTime':'now',
  // 	},
	// 	function(body){
	// 		console.log("Body:" + body);
	// 	}
	// );

	return result;
}

updateUserSeen = function(userID, seen, fs){
	//Get the current time in seconds
	var now = Math.floor(new Date() / 1000)

	if(typeof seen.users[userID] === 'undefined'){
		console.log("NEW USER...");
		var tempUser = {
			lastSeen: now
		}
		seen.users[userID] = tempUser;
		console.log("1: " + JSON.stringify(seen.users[userID]));
	}

	//Update the seen array
	seen.users[userID].lastSeen = now;
	console.log("2: " + JSON.stringify(seen.users[userID]));

	console.log("Users: ");
	seen.users.forEach(function(user, i) {
	    console.log(user + "|" + user.lastSeen);
	});

	//Save it to the local file
	/*fs.writeFile('seen.json',JSON.stringify(seen),function(err) {
	    if(err){
	    	console.log(err);
	    } else {
	    	console.log('Saved seen file hash as "seen.json"');
	    }
	});*/

	//Return the updated object
	return seen;
}

parseAdminCall = function(query){
	var result = "\n";

	//For testing, use the following parameters
	//?type=calladmin&name=L%20|%20Dracco&steamid=[U:1:139387111]&hostname=Dracco%27s%20Test%20Server&mapname=ctf_2fort&clientcount=1&maxclients=24&hostip=localhost&port=27015&reason=Testing

	result += ("Name:\n\t" + query.name + "\n");
	result += ("Steamid:\n\t" + query.steamid + "\n");
	result += ("Hostname:\n\t" + query.hostname + "\n");
	result += ("Mapname:\n\t" + query.mapname + "\n");
	result += ("Clientcount:\n\t" + query.clientcount + "\n");
	result += ("Maxclients:\n\t" + query.maxclients + "\n");
	result += ("Hostip:\n\t" + query.hostip + "\n");
	result += ("Port:\n\t" + query.port + "\n");
	result += ("Reason:\n\t" + query.reason + "\n");

	result =  query.name + " (" + query.steamid + ") requests admin assistance on " + query.hostname + " (" + query.mapname + ") [" + query.clientcount + "/" + query.maxclients + "] (steam://connect/" + query.hostip + ":" + query.port + ") Reason: " + query.reason;

	return result;
}

checkWatch = function(){
	var result = "";

	//Check to see if it is a steam

	return result;
}


postData = function(url, path, data, callback, sessionid, steamLogin){
	var querystring = require('querystring');
	var http = require('http');

	var post_data = querystring.stringify(data);

  	var fullCookie = ("sessionid=" + data.sessionid + "; steamLogin=" + data.steamLogin);

  	var post_options = {
      host: url,
      port: '80',
      path: path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': post_data.length,
      	  'Cookie': fullCookie
      }
  	};

  	var post_req = http.request(post_options, function(res) {
	  res.setEncoding('utf8');
	  var i = 0;
	  res.on('data', function (chunk) {
    	//console.log('Response: ' + chunk);
		callback(chunk, i);
		i++;
	  });
	});

	// post the data
	post_req.write(post_data);
	post_req.end();
}
