var _commands = [];
var commandNames = [];
var Permission = {
	"USER": 0,
	"ADMIN": 1,
	"STAFF": 2,
	"OWNER": 3
}
var AccessLevel = {
	0: "USER",
	1: "ADMIN",
	2: "STAFF",
	3: "OWNER"
}

var _users = [
	{	// L | Nano
		"SteamID64": "76561198045803147",
		"permission": Permission.STAFF
	},
	{	// L | Cactus
		"SteamID64": "76561198045000012",
		"permission": Permission.STAFF
	},
	{	// L | Skaila
		"SteamID64": "76561198022539561",
		"permission": Permission.STAFF
	},
	{	// L | Afronanny
		"SteamID64": "76561198010314894",
		"permission": Permission.STAFF
	},
	{	// L | Dr. Obvious
		"SteamID64": "76561197982228278",
		"permission": Permission.OWNER
	},
	{	// L | Mana
		"SteamID64": "76561197967203916",
		"permission": Permission.OWNER
	},
	{	// L | Dracco
		"SteamID64": "76561198099652839",
		"permission": Permission.OWNER
	},
	{	// LotusBotV2
		"SteamID64": "76561198180329801",
		"permission": Permission.OWNER
	}
];

//Handles all incomming chat messages
exports.handleChat = function (source, message, type, chatter) {
	//If the chatter is not set, set it to 0
	chatter = (chatter != undefined ? chatter : 0);

	var response = {
		"reply": "",
		"command": {
			"type": "",
			"SteamID64": ""
		}
	}
	var reply = "";

	//Check to see if there is a ! followed by an alphanumeric char
	var isCommand = (((message.substring(0, 1) == "!") && (message.substring(1, 2).match(/^[0-9a-zA-Z]+$/))) ? true : false);
	//console.log("First string: " + message.substring(0, 1));
	//console.log("Is command: " + isCommand);

	//If the chat is a command
	if(isCommand == true){
		//Parse out the command string
		message = message.substring(1, message.length);
		var command = message.match(/^(.{0,}?)(?:$|\s)/g)[0].trim().toLowerCase();
		var args = message.split(" ");
		args.shift();
		console.log("Command:" + command + " Args:" + args);

		//If that command is registered.
		if(_commands[command] != undefined){
			//Get the user's permission level
			var perm = getUserPermission(source, chatter);

			var tempCommand = _commands[command];

			//If the user has access to that command
			if(perm >= tempCommand.access){
				reply = tempCommand.response(command, args);
			}
			else {
				if(tempCommand.access == Permission.OWNER){
					//Don't reply to owner only commands
					reply = "";
				}
				else {
					//Reply telling the user they don't have the permission
					reply = "Sorry, you do not have access to that command";
				}
			}
		}
	}

	return reply;
}

regCommand(
	"ping",
	function(command, args) {
		return "PONG";
	},
	"Ping! Pong!",
	Permission.USER
);

regCommand(
	"pong",
	function(command, args) {
		return "PING";
	},
	"Pong! Ping!",
	Permission.USER
);


regCommand(
	"send",
	function(command, args) {
		newEvent(args);
		return "SEND";
	},
	"Testing stuff...",
	Permission.OWNER
);

regCommand(
	"8ball",
	function(command, args) {
		var answers = [
            "It is certain", "It is decidedly so", "Without a doubt", "Yes definitely",
            "You may rely on it", "As I see it, yes", "Most likely", "Outlook good",
            "Yes", "Signs point to yes", "Reply hazy, try again", "Ask again later",
            "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
            "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good",
            "Very doubtful", "No"
        ];
        var result = answers[Math.floor(Math.random()*answers.length)];
		return result;
	},
	"Magic 8 ball",
	Permission.USER
);

regCommand(
	"tell",
	function(command, args) {
		return ("Sending a message to the admin chat.");
	},
	"Sends a message to the admin chat.",
	Permission.USER
);

regCommand(
	"dice",
	function(command, args) {
		var die = Math.floor(6*Math.random())+1;
		return ("You rolled a " + die + ".");
	},
	"Rolls a 6-sided die and returns the result.",
	Permission.USER
);

regCommand(
	"admins",
	function(command, args) {
		var admins = [];
		for (var i = (0); i < _users.length; i++) {
			if(_users[i].permission >= Permission.ADMIN){
				admins.push(_users[i]);
			}
		}
		var result = "Xurrent Admins: \n";
		result += admins.map(function(elem){
		    return (steam64ToUsername(elem.SteamID64) + " ( http://steamcommunity.com/profiles/" + elem.SteamID64 + " )");
		}).join("\n");

		return result;
	},
	"Lists the SteamID64 of the admin group members.",
	Permission.STAFF
);

regCommand(
	"help",
	function(command, args) {
		var result = "\n";
		console.log("There are " + commandNames.length + " commands.");
		for (var i = 0; i < commandNames.length; i++) {
			var tempCommand = _commands[commandNames[i]];
			result += ("!" + tempCommand.trigger + "  \t[" + AccessLevel[tempCommand.access] + "] \t" + tempCommand.description + "\n");
		}
		return result;
	},
	"Lists all the available commands.",
	Permission.USER
);

//------------------------------------------------------//

function regCommand (trigger, response, description, access) {
	var tempCommand = {
		"trigger": trigger,
		"response": response,
		"description": description,
		"access": access
	}

	commandNames.push(trigger);
	_commands[trigger] = tempCommand;
}

function getUserPermission (source, chatter) {
	//Store the working permission
	var perm = 0;

	//If the command came from a private message
	if(source.substring(0, 1) != "1"){
		//This is a PM
		for (var i = 0; i < _users.length; i++) {
			var tempUser = _users[i];
			if(source == tempUser.SteamID64){
				perm = tempUser.permission;
				console.log("THIS IS THE USER!");
			}
		}
	}
	else {
		//This is a message from the admin chat

		//Check the chatter's permission
		perm = getUserPermission(chatter);

		perm = (perm >= Permission.ADMIN ? perm : Permission.ADMIN);
	}

	console.log("Permission:" + perm);
	return perm;
}

exports.getAdminList = function (http, parseString, SteamID) {
	var url = "http://steamcommunity.com/gid/" + SteamID + "/memberslistxml/?xml=1";
	var req = http.get(url, function(res) {
		var xml = '';
		res.on('data', function(chunk) {
			xml += chunk;
		});

		res.on('end', function() {
			//Parse in the loaded XML
			console.log("Finished loading the XML file...");
			parseString(xml, function (err, result) {
				var members = result.memberList.members[0].steamID64;

				//Loop over the list of admins
				for (var i = 0; i < members.length; i++) {
					var user = {
						"SteamID64": members[i].toString(),
						"permission": Permission.ADMIN
					}

					var exists = false;
					//If this user hasn't been added before
					for (var j = 0; j < _users.length; j++) {
						if(user.SteamID64 == _users[j].SteamID64){
							exists = true;
							break;
						}
					}

					if(exists == false){
						_users.push(user);
						//console.log("New:" + user.SteamID64);
					}
					else {
						//console.log("Exists:" + user.SteamID64);
					}
				}
				//console.log(_users);

				console.log("Finished adding " + _users.length + " admins to the admin list...");
			});
		});
	});

	req.on('error', function(err) {
		//Error loading in admin list
		console.log("Problem loading in the admin list...");
	});
}
