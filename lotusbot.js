var fs = require('fs');
var http = require('http');
var express = require("express");
var app = express();
var Steam = require('steam');
var parseString = require('xml2js').parseString;
var Settings = require('./Settings.js');
var Chat = require('./chatCommands.js');
var API = require('./API.js');
var readline = require('readline');
var request = require('sync-request');

//Global variables
var _config = {};
var _seen = {};
var _cookies = {};

// if we've saved a server list, use it
if (fs.existsSync('servers')) {
  Steam.servers = JSON.parse(fs.readFileSync('servers'));
}

// if we've saved a config file, use it
if (fs.existsSync('config.json')) {
  _config = JSON.parse(fs.readFileSync('config.json'));
}

// if we've saved a seen file, use it
if (fs.existsSync('seen.json')) {
  _seen = JSON.parse(fs.readFileSync('seen.json'));
}

// Setup readline to read from console.  This is used for Steam Guard codes.
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//Try to login with the loaded credentials
var bot = new Steam.SteamClient();
bot.logOn({
  accountName: _config.steam.username,
  password: _config.steam.password,
  shaSentryfile: (fs.existsSync('sentryfile') ? fs.readFileSync('sentryfile') : undefined)
});

bot.on('loggedOn', function() {
  console.log('Logged in!');
  //Set the bot's state to "online"
  bot.setPersonaState(Steam.EPersonaState.Online);

  //Set it's nickname
  bot.setPersonaName(_config.steam.displayname);

  //Join the admin chat room
  bot.joinChat(_config.steam.adminRoomID);

  //Get the admin list
  Chat.getAdminList(http, parseString, _config.steam.adminRoomID);
});

bot.on('servers', function(servers) {
  //Save the steam server list to a local file.
  fs.writeFile('servers', JSON.stringify(servers));
});

bot.on('chatInvite', function(chatRoomID, chatRoomName, patronID) {
  console.log('Got an invite to ' + chatRoomName + ' from ' + bot.users[patronID].playerName);
  bot.joinChat(chatRoomID); // autojoin on invite
});

bot.on('message', function(source, message, type, chatter) {
  // respond to both chat room and private messages
  console.log('Received message(' + source + '): ' + message);

  //If the incomming message was not blank
  if(message != ""){

    //Testing...
    //console.log(steam64ToUsername(source));

    //Get the response from the chat engine
    var reply = Chat.handleChat(source, message, type, chatter);

    //If we actually want to send a message back
    if (reply != ""){
      bot.sendMessage(source, reply, Steam.EChatEntryType.ChatMsg);
    }
  }
  else {
    //"source" is typing us a message
    //Yeah, that's actually creepy...
    //Not sure what I can do with this. but yeah, it's there...
  }
});

bot.on('chatStateChange', function(stateChange, chatterActedOn, steamIdChat, chatterActedBy) {
  //If the bot was just kicked from a chat room
  if (stateChange == Steam.EChatMemberStateChange.Kicked && chatterActedOn == bot.steamID) {
    bot.joinChat(steamIdChat);  // autorejoin!
  }

  //If something happened in the admin chat room
  if(steamIdChat === _config.steam.adminRoomID){

    //If someone did something to themselves
    if(chatterActedOn == chatterActedBy){

     // bot.sendMessage(steamIdChat, ("Start:" + JSON.stringify(_seen)), Steam.EChatEntryType.ChatMsg);

      //Log the time in the seen.json file
      _seen = updateUserSeen(chatterActedOn, _seen, fs);

      var greeting = "";

      //If a user just joined
      if(stateChange == 1){
        greeting = "Hello, ";
      }
      //If a user just left
      if(stateChange == 2){
        greeting = "Goodbye, ";
      }

      greeting += steam64ToUsername(chatterActedOn);

      //bot.sendMessage(steamIdChat, ("End:" + JSON.stringify(_seen)), Steam.EChatEntryType.ChatMsg);
      bot.sendMessage(steamIdChat, greeting, Steam.EChatEntryType.ChatMsg);
    }

    //console.log(chatterActedOn + " was " + stateChange + " by " + chatterActedBy);
  }

});

bot.on('announcement', function(group, headline) {
  console.log('Group with SteamID ' + group + ' has posted ' + headline);
});

bot.on('error', function(e) {
  if (e.eresult == Steam.EResult.AccountLogonDenied) {
    // Prompt the user for Steam Gaurd code
    rl.question('Steam Guard Code: ', function(code) {
      // Try logging on again
      bot.logOn({
          accountName: _config.steam.username,
          password: _config.steam.password,
          authCode: code
      })
;    });
  } else {
    // For simplicity, we'll just log anything else.
    // A list of ENUMs can be found here:
    // https://github.com/SteamRE/SteamKit/blob/d0114b0cc8779dff915c4d62e0952cbe32202289/Resources/SteamLanguage/eresult.steamd

    console.log("ERROR: " + e.cause + "|" + e.eresult);
  }
});

bot.on('sentry',function(sentryHash) {
  fs.writeFile('sentryfile',sentryHash,function(err) {
    if(err){
      console.log(err);
    } else {
      console.log('Saved sentry file hash as "sentryfile"');
    }
  });
});

//When we are sent a friend request
bot.on('friend',function(source, status) {
  if(status == EClanRelationship.Invited){
    console.log("Accepting friend: " + source);
    bot.addFriend(source);
  }
});

//When we have recieved a webSessionID
bot.on('webSessionID',function(sessionID) {
  console.log("New sessionID: " + sessionID);

  //Request new cookies
  bot.webLogOn(function(cookies){
    //Parse out the cookies into an object
    _cookies = parseCookies(cookies);
    //console.log(_cookies);

    //We now have the required cookies for our user

    //Send the form update request, using our newly recieved cookies
    //var tempForm = newEvent(_cookies);
    console.log("SEND FAKE STARTING EVENT");
  });
});


//SERVER RESPONSE THINGS
app.get('/', function (req, res) {

  var type = req.query.type;

  console.log(req.query);
  res.send(req.query);
  return;

  //Check to see the type of the connection
  if(req.query.type != undefined){

    console.log("Type: " + type);

    switch(type){

      case "calladmin":
        //Parse the admin call
        var response = parseAdminCall(req.query);

        bot.sendMessage(_config.steam.adminRoomID, response, Steam.EChatEntryType.ChatMsg);
        break;

      case "watch":

        break;

      case "voteCancel":

        break;
    }

    //Return the message that was recieved back.
    res.send(req.query);
  }
  else {

    //Legacy calladmin checking
    if(req.query.message != undefined){
      var message = req.query.message;

      console.log("Message: " + message);

      //Send the message to the admin chat
      bot.sendMessage(_config.steam.adminRoomID, message, Steam.EChatEntryType.ChatMsg);

      //Return the message that was recieved back.
      res.send(req.query.message);
    }
    else {
      //Just in case something went wrong, return something
      res.send("Something went wrong.");
    }
  }

  console.log("New request:");
  console.log(req.query);

})

var _port = (_config.env.isProd ? prodPort : port);

console.log("Using port:" + _port);

var server = app.listen(_port, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Calladmin listening at http://%s:%s', host, port)
})



/*
  TODO:
    - Add in logging
    - Add in more response types (line 52)
    - Rework how calls are sent to the bot (seperate out the parameters)

  Fun:
    - Cleverbot
    - Youtube
*/
